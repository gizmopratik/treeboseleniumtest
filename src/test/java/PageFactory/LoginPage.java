package PageFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;
 


public class LoginPage {
 
    /**
     * All WebElements are identified by @FindBy annotation
     */
 
    WebDriver driver;
 
    @FindBy(id="signinemailAddress")
    WebElement emailId;
 
    @FindBy(name="password")
    WebElement password;

    @FindBy(name="headerLogin")
    WebElement headerLogin;
    
    
    @FindBy(name="popupLogin")
    WebElement loginButton;
 
    public LoginPage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
 

    //Set user name in textbox
    public void setUserName(String strUserName) throws InterruptedException{
    	Thread.sleep(5000);
    	//emailId.click();
    	emailId.sendKeys(strUserName);
    }
 
    //Set password in password textbox
    public void setPassword(String strPassword){
    	password.sendKeys(strPassword);
    }
 
    //Click on login button
    public void clickLogin(){
    	loginButton.click();
    }
 
    /**
     * This POM method will be exposed in test case to login in the application
     * @param strUserName
     * @param strPasword
     * @return
     * @throws InterruptedException 
     */

    public void loginToTreebo(String strUserName,String strPasword) throws InterruptedException{


		headerLogin.click();
		ExpectedConditions.visibilityOf(emailId);
    	//Fill user name
        this.setUserName(strUserName);
        //Fill password
        this.setPassword(strPasword);
 
        //Click Login button
        this.clickLogin();
    	Thread.sleep(10000);
    }
    
    public void loginToTreeboMobileWeb(String strUserName,String strPasword) throws InterruptedException{


		headerLogin.click();
		ExpectedConditions.visibilityOf(emailId);
    	//Fill user name
        this.setUserName(strUserName);
        //Fill password
        this.setPassword(strPasword);
 
        //Click Login button
        this.clickLogin();
    	Thread.sleep(10000);
    }
    
 
}