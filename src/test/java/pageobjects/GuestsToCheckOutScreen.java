package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class GuestsToCheckOutScreen {
  private WebDriver driver;

  private By guestsToCheckOutScreenTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/toolbar_title'][@text='GUESTS TO CHECK OUT']");

  private By selectAllCheckOutBox = By.xpath("//android.widget.CheckBox[@resource-id='com.treebo.bumblebee:id/header_checkbox']");

  private By continueButtonInGuestsToCheckoutScreen = By.xpath("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/footer_btn'][@text='Continue']");

  public GuestsToCheckOutScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyGuestsToCheckOutScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(guestsToCheckOutScreenTitle));
    Assert.assertTrue(driver.findElement(guestsToCheckOutScreenTitle).isDisplayed());
  }

  public void verifyContinueButtonIsDisabledIfNoGuestsSelected(){
      WebDriverWait wait = new WebDriverWait(driver, 60000);
      wait.until(ExpectedConditions.presenceOfElementLocated(continueButtonInGuestsToCheckoutScreen));
      Assert.assertTrue(!driver.findElement(continueButtonInGuestsToCheckoutScreen).isEnabled());
    }

  public void selectAllCheckboxForCheckOut(){
    driver.findElement(selectAllCheckOutBox).click();
  }

  public void selectGuestsByRoomAndGuestNumber(String[][] roomAndGuestsToCheckOut){
    //Sample array of array roomAndGuestsToCheckOut = {{"1","2"},{"2","2"}}
    //In room 1, guest 2 to checkout and in room2 , guest 2 to checkout
    //xpath= //android.support.v7.widget.RecyclerView[@resource-id='com.treebo.bumblebee:id/checkout_recycler']/
    //        android.widget.RelativeLayout[roomIndex]/android.widget.RelativeLayout/android.widget.LinearLayout[guestIndex]/
    //        android.widget.CheckBox
    for (String[] room : roomAndGuestsToCheckOut){
      for (int i=1;i<=(room.length - 1);i++){
        String xpathGuestCheckbox = String.format("//android.support.v7.widget.RecyclerView[@resource-id='com.treebo.bumblebee:id/checkout_recycler']/android.widget.RelativeLayout[%s]/android.widget.RelativeLayout/android.widget.LinearLayout[%s]/android.widget.CheckBox",room[0],room[i]);
        driver.findElement(By.xpath(xpathGuestCheckbox)).click();
      }
    }
  }

  public void clickOnContinueButtonInGuestsToCheckoutScreen(){
    Assert.assertTrue(driver.findElement(continueButtonInGuestsToCheckoutScreen).isEnabled());
    driver.findElement(continueButtonInGuestsToCheckoutScreen).click();
  }
}
