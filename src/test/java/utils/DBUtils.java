package utils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

import base.DatabaseConnection;

public class DBUtils {
	
	public Map<String, Integer> getRoomAvailability(String date) throws SQLException {

		// DB credentials
		// String server = "172.40.20.210";
		// String databaseName = "ashishmantri";
		// String user = "ashishmantri";
		// String PGPASSWORD = "ashishmantri";

		String server = "treebo-pg-master.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };

		// String date = "2016-04-30";

		String sql = String.format(
				"SELECT hotelogix_name,room_type,availablerooms FROM bookingstash_availability INNER JOIN hotels_room ON bookingstash_availability.room_id = hotels_room.id and bookingstash_availability.date = '%s' INNER JOIN hotels_hotel ON hotels_hotel.id = hotel_id order by hotelogix_name asc",
				date);

		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Map<String, Integer> dbRoomAvailability = new ConcurrentSkipListMap<String, Integer>();
		while (itr.hasNext()) {
			String[] strArr = itr.next().toString().split(",");
			String hotelName = strArr[0].replaceAll("\\[", "").trim();
			if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
				String roomType = strArr[1].trim();
				int available = Integer.parseInt(strArr[2].replaceAll("\\]", "").trim());
				dbRoomAvailability.put(hotelName + "_" + roomType, available);
			}
		}
		// System.out.println(dbRoomAvailability.keySet().toString());
		dc.close();
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		// for (String key : dbRoomAvailability.keySet()) {
		// System.out.println(key + " " + dbRoomAvailability.get(key));
		// }
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		return dbRoomAvailability;
	}

	public Map<String, Integer> getRoomAvailabilityByHotel(String strHotelName, String date) throws SQLException {

		// DB credentials
		// String server = "172.40.20.210";
		// String databaseName = "ashishmantri";
		// String user = "ashishmantri";
		// String PGPASSWORD = "ashishmantri";

		String server = "treebo-pg-master.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };

		// String date = "2016-04-30";

		String sql = String.format(
				"SELECT hotelogix_name,room_type,availablerooms FROM bookingstash_availability INNER JOIN hotels_room ON bookingstash_availability.room_id = hotels_room.id and bookingstash_availability.date = '%s' INNER JOIN hotels_hotel ON hotels_hotel.id = hotel_id order by hotelogix_name asc",
				date);

		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Map<String, Integer> dbRoomAvailability = new ConcurrentSkipListMap<String, Integer>();
		while (itr.hasNext()) {
			String[] strArr = itr.next().toString().split(",");
			String hotelName = strArr[0].replaceAll("\\[", "").trim();
			if ((hotelName.toLowerCase()).contains(strHotelName.toLowerCase())) {
				if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
					String roomType = strArr[1].trim();
					int available = Integer.parseInt(strArr[2].replaceAll("\\]", "").trim());
					dbRoomAvailability.put(hotelName + "_" + roomType, available);
				}
			}
		}
		// System.out.println(dbRoomAvailability.keySet().toString());
		dc.close();
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		// for (String key : dbRoomAvailability.keySet()) {
		// System.out.println(key + " " + dbRoomAvailability.get(key));
		// }
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		return dbRoomAvailability;
	}

	public Map<String,String> getOrderIdAndReservationId(String email,String date) throws SQLException {		
		String server = "treebo-pg-master.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String sql = String.format(
				"select bookings_booking.order_id,bookings_roombooking.reservation_id from bookings_booking INNER JOIN bookings_roombooking ON bookings_booking.id=bookings_roombooking.booking_id where Lower(bookings_booking.guest_name) like '%%test%%' AND bookings_booking.status='1' AND bookings_booking.created_at >= '%s';",date);
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Map<String,String> orderAndReservationId = new HashMap<String,String>();
		while (itr.hasNext()) {
			String order[] = itr.next().toString().replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			orderAndReservationId.put(order[0].trim(), order[1].trim());
		}
		dc.close();
		return orderAndReservationId;
	}
	
	/**
	 * Returns 
	 * id,order_id,checkin_date,checkout_date,adult_count,child_count
	 * total_amount,guest_name,guest_email,guest_mobile,payment_amount
	 * comments, 
	 * coupon_apply,coupon_code,discount,payment_mode
	 * status
	 * pretax_amount,tax_amount
	 * channel
	 * 
	 * @param bookingId
	 * @param hotelId
	 * @return
	 * @throws SQLException
	 */
	
	public Map<String,String> getBookingInfo(String bookingId, String hotelId) throws SQLException{
		String server = "treebo-pg-master.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";
		
		String sql = null;
		String sqlComment = null;
		String spReq = null;
		if (bookingId.contains("G")){
			sql = String.format("select * from bookings_booking where group_code='%s' AND hotel_id='%s';",bookingId,hotelId);
			sqlComment = String.format("select comments from bookings_booking where group_code='%s' AND hotel_id='%s';",bookingId,hotelId);
		}else{
			sql = String.format("select * from bookings_booking where booking_code='%s|' AND hotel_id='%s';",bookingId,hotelId);
			sqlComment = String.format("select comments from bookings_booking where booking_code='%s|' AND hotel_id='%s';",bookingId,hotelId);
		}
		
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Iterator itr1 = dc.executeQuery(sqlComment).iterator();
		while (itr1.hasNext()) {
			String spReqStr = itr1.next().toString();
			spReq = spReqStr.substring(1, spReqStr.length() - 1);
		}
		
		Map<String,String> orderInfoInDB = new HashMap<String,String>();
		while (itr.hasNext()) {
			String[] order = itr.next().toString().replace(spReq,"").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			orderInfoInDB.put("id", order[0].trim());
			orderInfoInDB.put("order_id", order[3].trim());
			orderInfoInDB.put("checkin_date", order[4].trim());
			orderInfoInDB.put("checkout_date", order[5].trim());
			orderInfoInDB.put("adult_count", order[6].trim());
			orderInfoInDB.put("child_count", order[7].trim());
			orderInfoInDB.put("total_amount", order[8].trim());
			orderInfoInDB.put("guest_name", order[9].trim());
			orderInfoInDB.put("guest_email", order[10].trim());
			orderInfoInDB.put("guest_mobile", order[11].trim());
			orderInfoInDB.put("payment_amount", order[17].trim());
			orderInfoInDB.put("comments", spReq.trim());
			orderInfoInDB.put("coupon_apply", order[20].trim());
			orderInfoInDB.put("coupon_code", order[21].trim());
			orderInfoInDB.put("discount", order[22].trim());
			orderInfoInDB.put("payment_mode", order[23].trim());
			orderInfoInDB.put("status", order[25].trim());
			orderInfoInDB.put("pretax_amount", order[27].trim());
			orderInfoInDB.put("tax_amount", order[28].trim());
			orderInfoInDB.put("channel", order[30].trim());
			orderInfoInDB.put("is_audit", order[31].trim());
		}
		dc.close();
	    System.out.println(orderInfoInDB.toString());
		return orderInfoInDB;
	}

	public boolean stringContainsItemFromArray(String inputString, String[] items) {
		for (int i = 0; i < items.length; i++) {
			if (inputString.contains(items[i])) {
				return true;
			}
		}
		return false;
	}
}
