package base;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestBaseSetUp {

	@BeforeMethod(alwaysRun = true)
	public void initTest() {
		try {
			WebDriver driver;
			try{
				driver = DriverManager.getInstance().getDriver();
			}catch(Exception SessionNotCreatedException){
				driver = DriverManager.getInstance().getDriver();
			}
			
			String host = System.getProperty("host");
			System.out.println("Starting test ...");

			// get thread safe driver from driver Manager for specific browser
			// etc
			// if Desktop/Mobile browser maximize it or set the dimension
			// Get the test URL and open it

			String url = System.getProperty("url");
			if (url == null || url.isEmpty()) {
				url = "http://treebohotels.com";
				System.out.println("Url not provided! Default url is : " + url);
			} else {
				System.out.println("Launching website : " + url);
			}

			driver.get(url);
			if (host == null || host.isEmpty()) {
				host = "grid";
			}
			if (host.equals("localhost")) {
				System.out.println("Maximizing the browser window for localhost");
				driver.manage().window().maximize();
			} else if (host.equals("grid")) {
				System.out.println("Maximizing the browser window with dimension for remote machine, grid");
				Dimension dimension = new Dimension(1920, 1080);
				driver.manage().window().setSize(dimension);
			}
			// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(false, "Error in setting up driver!");
		}

	}

	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		System.out.println("Test completed is: " + result.getMethod().getMethodName());
		System.out.println("Test Passed? " + result.isSuccess());
	}

	@AfterMethod(alwaysRun = true)
	public void endTest() {
		// for Desktop Browser and Mobile Browser(Through Chrome options)
		// quit the threadlocal driver and remove it
		if (!(System.getProperty("debug").equals("true"))){
			DriverManager.getInstance().removeDriver();
		}
	}

}