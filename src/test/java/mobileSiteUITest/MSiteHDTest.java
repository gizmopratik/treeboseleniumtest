package mobileSiteUITest;

import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;

import base.TestBaseSetUp;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import utils.CommonUtils;
import mobileSiteUIPageObjects.HotelDetailsScreen;

public class MSiteHDTest extends TestBaseSetUp {

	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private HotelDetailsScreen hotelDetailsScreen;

	/**
	 * This test verifies that User can modify search from HD screen
	 * Dates/Room Type/Room config
	 */
	@Test(groups = { "MSite", "MSiteHD" })
	public void testSelectOtherRoomTypeThroughModifyWidget() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("city"); // "Bengaluru";
		String hotelNameWithDiffRoomType = utils.getProperty("HotelNameAllRoomType"); //Treebo Akshaya Mayflower
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		
		// Check if home screen is displayed
				verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
				
       // Do search
		homeScreen.doSearch(location,checkInFromCurrentDate,checkOutFromCurrentDate);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		
		// Click Specific hotel which has different room types and go to HD screen
		hotelSearchResultScreen.clickHotelByName(hotelNameWithDiffRoomType);
		// Verify HD page displayed
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(), "HD Screen is not displayed");

		// Modify Room Type
		String[] roomTypeInHotel = hotelDetailsScreen.getAllRoomType();
		String selectedRoomType = hotelDetailsScreen.getSelectedRoomType();
		
		List<String> roomTypeToSelect = Arrays.asList(roomTypeInHotel);
		//roomTypeToSelect.remove(selectedRoomType);
		
		// Open modify widget
		hotelDetailsScreen.openModifyWidget();
		String roomToSelect = roomTypeToSelect.get(1);
		hotelDetailsScreen.modifyRoomTypeForSelectedHotel(roomToSelect);
		hotelDetailsScreen.clickDoneInModify();
		
		// Now selected room should be different than originally and equal to the one which was selected
		verify.assertNotEquals(hotelDetailsScreen.getSelectedRoomType(), selectedRoomType, "Room type is still the initial one!");
		verify.assertEquals(hotelDetailsScreen.getSelectedRoomType(), roomToSelect,"Wrong room type selected!");
		
		verify.assertAll();
	}
	
	
	/**
	 * This test verifies that full screen image gallery
	 */
	
	/**
	 * Select hotel from Similar Hotel
	 */
	@Test(groups = { "MSite", "MSiteHD" })
	public void testSelectSimilarHotelLoadsHD() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("city"); // "Bengaluru";
		
		// Check if home screen is displayed
				verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
				
       // Do search
		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		
		// Click first hotel and go to HD screen
		hotelSearchResultScreen.clickHotelByIndex(1);
		// Verify in HD page similar treebos displayed
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(), "HD Screen is not displayed");
		verify.assertTrue(hotelDetailsScreen.isSimilarTreebosDisplayed(), "Similar Treebos not displayed");
		
		String similarTreeb = hotelDetailsScreen.getNameOfCurrentSimilarTreebo();
		hotelDetailsScreen.clickCurrentSimilarTreebo();
		
		// Verify in HD page similar treebos displayed
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(), "HD Screen is not displayed");
		verify.assertTrue(hotelDetailsScreen.isSimilarTreebosDisplayed(), "Similar Treebos not displayed");
		
		// Verify the name clicked in Similar hotel is loaded
		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), similarTreeb,"Hotel name does not match!");
		verify.assertAll();
	}
}
