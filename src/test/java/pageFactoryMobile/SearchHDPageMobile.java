package pageFactoryMobile;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;

public class SearchHDPageMobile {

	/**
	 * All WebElements are identified by @FindBy annotation
	 */

	SelendroidDriver driver = null;

	@FindBy(className = "btn-primary")
	WebElement book_button;

	@FindBy(className = "copyright")
	WebElement copyright;

	@FindBy(className = "room-price")
	WebElement room_price;

	@FindBy(className = "block-title")
	WebElement about_this_hotel;
	
	public SearchHDPageMobile(SelendroidDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
	}

	public void click_on_book_button() throws InterruptedException {

		// Thread.sleep(2000);

		driver.findElement(By.xpath("//*[@id='hotelDetails']/div[2]/div[1]/div/div[2]/h3")).click();
		Thread.sleep(3000);

		// driver.findElement(By.cssSelector("#hotelDetails > div:nth-child(2) >
		// div:nth-child(1) > div > div.room-detail > div.room-book > a")).click();
		// while(driver.findElement(By.className("fa-times")).isDisplayed())
		// {
		// Thread.sleep(3000);
		// room_price.click();
		// Thread.sleep(3000);
		// Alternate to primary book button --> driver.findElement(By.className("btn-primary")).click();
		
		driver.findElement(By.className("fa-times")).click();
		Thread.sleep(3000);

//		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		 driver.findElement(By.cssSelector("#hotelDetails > div:nth-child(2) > div:nth-child(1) > div > div.room-detail > div.room-book > a")).click();
		 Thread.sleep(3000);
	    	System.out.println("Leaving HD Page page");
		 // }

	}

}