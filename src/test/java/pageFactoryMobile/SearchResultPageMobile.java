  
    

package pageFactoryMobile;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;
 



 
public class SearchResultPageMobile{
	 
    /**
     * All WebElements are identified by @FindBy annotation
     */
 
	SelendroidDriver driver;
 
    
    @FindBy(css="#h14 > div.hotel-info.pos-rel.hand > a > img")
    WebElement first_result;
    
    @FindBy(className="fa-search")
    WebElement search_button;

    @FindBy(className="fa-sort_select")
    WebElement sort_select;
    
    @FindBy(id="minSearchBox")
    WebElement minSearchBox;
    
 
    public SearchResultPageMobile(SelendroidDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
 
    
    public void click_on_first_result() throws InterruptedException{
    	System.out.println("Entering search result page");
//    	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
    	first_result.click();
    	Thread.sleep(5000);
    	System.out.println("Leaving search result page");
    }
   
}