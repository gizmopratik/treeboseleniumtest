package TreeboUI;

import org.testng.AssertJUnit;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;
import PageFactory.HomePage;
import PageFactory.ITCPage;
import PageFactory.LoginPage;
import PageFactory.NewUserRegistrationPage;
import DataProvider.DataProviderClass;
import org.testng.Assert;

public class BookHotelAsGuestTest {


	WebDriver driver;
	LoginPage objLogin;
	HomePage objHome;
	ITCPage objITC;
	NewUserRegistrationPage objNewUserRegistration;
	
	@BeforeTest()
	public void setup() {
		// Chrome Driver
		System.setProperty("webdriver.chrome.driver", "/Users/ashishmantri1/Downloads/chromedriver");

		// driver = new ChromeDriver();
		driver = new FirefoxDriver();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://treebohotels.com");
		driver.manage().window().maximize();
	}

	/**
	 * This test go to http://treebohotels.com Verify login page title as
	 * toashishmantriATgmail.com Login to application
	 **/

	@Test(priority=1)
	public void scrollingToBottomofAPage() {
		driver.navigate().to("https://www.linkedin.com/");
		 ((JavascriptExecutor) driver)
         .executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	
//	@Test(priority = 0)
	public void test_book_a_hotel() throws InterruptedException {

		// Create Login Page object
		objLogin = new LoginPage(driver);
		objHome = new HomePage(driver);
		objITC = new ITCPage(driver);
		objNewUserRegistration = new NewUserRegistrationPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver, 10);

		// Verify if the search button is present or not.
		AssertJUnit.assertEquals(objHome.checksearchbutton(), true);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Home");

		driver.findElement(By.className("inputText")).sendKeys("GOA");

		// Do an Empty search
		objHome.clickSearchButton();

		// Verify if user reaches on the search page.
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Search");

		driver.findElement(By.className("copy-rights")).click();
		
		// Click on view details for the first hotel and click on room
		driver.findElement(By.id("searchedHotelsDetail")).click();

		// CLick on 3rd button (book a treebo)
		driver.findElement(By.name("hotelBookNow")).click();

		// Verify if user reaches on the Itinerary Section.
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Confirmation");
		Thread.sleep(30000);
		driver.findElement(By.id("continue-as-guest")).click();

		// Verify if user reaches on the Travelers Section.
		Thread.sleep(30000);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Traveller");
		objITC.enterGuestInformationOnConfirmationPage("Dummy Test", objNewUserRegistration.generateRandomEmail(), "9876543210");

		
		driver.findElement(By.className("pay_at_hotel")).click();

		// Verify if user reaches on the Confirmation Section.
		Thread.sleep(30000);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Order");
	}
}
