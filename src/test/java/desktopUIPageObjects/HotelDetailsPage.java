package desktopUIPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.DriverManager;
import desktopUIPageObjects.HomePage;
import utils.CommonUtils;

public class HotelDetailsPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;

	private By hotelDetailsPage = By.xpath("//div[@class='hotel-detail-page']");
	private By hotelName = By.xpath("//div[@class='hdp__header']/div[contains(@class,'main__title')]/h1");
	private By hotelAddress = By.xpath("div[@class='main__address']/span");
	private By hotelImagesInHD = By.xpath(
			"//div[contains(@class,'main-gallery__carousel')]//div[contains(@class,'main-gallery__carousel__item')]/img");

	private By bookNowButton = By.xpath("//button[contains(@class,'analytics-book')]");
	private By bookNowButtonEnabled = By.cssSelector("button.analytics-book:not(.sidebar__book-disabled)");
	private By bookNowButtonDisabled = By.cssSelector("button.analytics-book.sidebar__book-disabled");
	private By selectRoomButton = By.cssSelector(".analytics-select:not(.hide)");
	private By totalPriceDisplaedInSideRack = By.xpath("//span[contains(@class,'analytics-totalprice')]");
	private By fareBreakUp = By.xpath("//div[@id='price-popup']/div[contains(@class,'farebreakuptitle')]");
	private By roomPriceInBreakUp = By.xpath("//div[text()='Room Price']/following-sibling::div/span[2]");
	private By taxesInBreakUp = By.xpath("//div[text()='Taxes']/following-sibling::div/span[2]");
	private By discountInBreakUp = By.xpath("//div[text()='Discount']/following-sibling::div/span[3]");
	private By totalInBreakUp = By.xpath("//div[text()='Total']/following-sibling::div/span/span");

	private By calendarLeftIcon = By.xpath(
			"//div[@class='dr-calendar'][not(contains(@style,'none'))]//i[contains(@class,'dr-left')][not(contains(@class,'dr-disabled'))]");
	private By checkin = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-start search-box__title')]");

	private By applyCouponCodeLink = By.cssSelector(".js-applycoupon");
	private By inputCoupon = By.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__voucher')]");
	private By applyCouponButton = By
			.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__applybtn')]");
	private By discountAppliedValue = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-discountvalue");
	private By couponApplied = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-coupon");
	private By removeCoupon = By.cssSelector(".discount__applied-screen:not(.hide) .text-right a");
	private By couponInvalidError = By.xpath("//div[@class='apply-screen']//div[@id='discountError']");

	public void verifyHotelDetailsPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(hotelDetailsPage));
		Assert.assertTrue(driver.findElement(hotelName).isDisplayed());
		Assert.assertTrue(driver.findElement(bookNowButton).isDisplayed());
		System.out.println("Hotel Details Page is Displayed");
	}

	public String getHotelNameInHotelDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelName).getText().trim();
	}

	public String getHotelAddressInHotelDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelAddress).getText();
	}

	public void clickBookNowInHotelsDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("clicking on book now in Hotel details page");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(bookNowButton));

		browserHelper.jsclick(driver.findElement(bookNowButtonEnabled));
		System.out.println("Leaving Hotel Details Page...");
	}

	public void ensureRoomAvailableInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (!isBookNowButtonEnabled()) {
			if (driver.findElement(By.cssSelector(".sidebar__error")).getText()
					.equals("No of guests is greater than room capacity")) {
				Assert.assertTrue(false,"The selected room does not support room config");
			}
			System.out.println(
					"Book Now Button is disabled in HD page, trying to select either other room available or change date...");
			changeRoomOrDatesIfBookNowIsDisabled();
		}
	}

	public boolean isBookNowButtonDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(bookNowButton));
		return driver.findElement(bookNowButton).isDisplayed();
	}

	public boolean isBookNowButtonEnabled() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean flag = false;
		if (driver.findElements(bookNowButtonEnabled).size() > 0) {
			System.out.println("Book Now Button is enabled");
			flag = true;
		}
		if (driver.findElements(bookNowButtonDisabled).size() > 0) {
			System.out.println("Book Now Button is disabled");
			flag = false;
		}
		return flag;
	}

	public String[] getAllRoomTypesAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver.findElements(By.cssSelector(".room-types__room"));
		System.out.println(("number of rooms :" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("Room Types in this hotel are :" + Arrays.toString(roomType));
		return roomType;
	}

	public boolean selectRoomType(String roomType) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean roomAvailable;
		String selectRoomTypeButton = String.format("div[roomtype=%s] .analytics-select:not(.hide)", roomType);
		if ((roomType == "Mahogany")
				&& (driver.findElements(By.cssSelector(".room-types .slick-next.slick-arrow")).size() > 0)) {
			driver.findElement(By.cssSelector(".room-types .slick-next.slick-arrow")).click();
		}
		if (driver.findElements(By.cssSelector(selectRoomTypeButton)).size() > 0) {
			roomAvailable = true;
			driver.findElement(By.cssSelector(selectRoomTypeButton)).click();
		} else {
			roomAvailable = false;
		}
		return roomAvailable;
	}

	public void changeRoomOrDatesIfBookNowIsDisabled() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		if (!isBookNowButtonEnabled()) {
			if (isAnyRoomTypeAvailable()) {
				driver.findElement(selectRoomButton).click();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				// change the date to next day
				int checkIn = utils.getDaysDiffFromSelectedDateToCurrentDate(homePage.getEnteredCheckInDate());
				int checkOut = utils.getDaysDiffFromSelectedDateToCurrentDate(homePage.getEnteredCheckOutDate());
				int updatedCheckIn = checkIn + 2;
				int updatedCheckOut = checkOut + 2;
				System.out.println("******" + checkIn + checkOut + updatedCheckIn + updatedCheckOut);
				changeDatesFromHDPage(updatedCheckIn, updatedCheckOut);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				changeRoomOrDatesIfBookNowIsDisabled();
			}
		}
	}

	// checkin days from current date, and same way checkout days from current
	// date
	public void changeDatesFromHDPage(int checkIn, int checkOut) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(checkin).click();
		// reset the calendar to current month
		while (driver.findElements(calendarLeftIcon).size() > 0) {
			driver.findElement(calendarLeftIcon).click();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		homePage.selectDate(checkIn);
		homePage.selectCheckOutDate(checkIn, checkOut);
	}

	public boolean isAnyRoomTypeAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(selectRoomButton).size() > 0);
	}

	public int getHotelTotalPriceInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int totalPriceInHD = Integer.parseInt(driver.findElement(totalPriceDisplaedInSideRack).getText());
		System.out.println("Total Price Displayed in HD Page :" + totalPriceInHD);
		return totalPriceInHD;
	}

	public void mouseOverPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.mouseOver(driver.findElement(fareBreakUp));
	}

	public void mouseOutPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.mouseOver(driver.findElement(hotelName));
	}

	public double[] getHotelPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		double[] hotelPriceInfo = new double[4];
		mouseOverPriceBreakUpInHD();
		hotelPriceInfo[0] = Double.parseDouble(driver.findElement(roomPriceInBreakUp).getText());
		hotelPriceInfo[1] = Double.parseDouble(driver.findElement(taxesInBreakUp).getText());
		hotelPriceInfo[2] = Double.parseDouble(driver.findElement(discountInBreakUp).getText());
		hotelPriceInfo[3] = Double.parseDouble(driver.findElement(totalInBreakUp).getText());
		mouseOutPriceBreakUpInHD();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("HD Page Price details : " + "Room price : " + hotelPriceInfo[0] + " Taxes : "
				+ hotelPriceInfo[1] + " Discount :" + hotelPriceInfo[2] + " Total : " + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}

	public boolean isPriceCalcultaionInBreakUpCorrectInHDPage() {
		double[] hotelPriceInfo = getHotelPriceBreakUpInHD();
		System.out.println("Should be equal :" + hotelPriceInfo[3] + " and "
				+ (hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
		return ((int) hotelPriceInfo[3] == (int) Math.round(hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
	}

	public boolean isTotalInBreakUpAndRoomRateEqualInHDPage() {
		int roomRate = getHotelTotalPriceInHD();
		int totalInBreakUp = (int) getHotelPriceBreakUpInHD()[3];
		System.out.println("Room rate is : " + roomRate + " and total in break up is : " + totalInBreakUp);
		return (roomRate == totalInBreakUp);
	}

	public void applyCouponInHDPage(String coupon) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Applying coupon in HD page :" + coupon);
		driver.findElement(applyCouponCodeLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(inputCoupon));
		driver.findElement(inputCoupon).sendKeys(coupon);
		driver.findElement(applyCouponButton).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getCouponDiscountDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int discountApplied = (int) Math.round(Double.parseDouble((driver.findElement(discountAppliedValue).getText())));
		System.out.println("Discount coupon applied :" + discountApplied);
		return discountApplied;
	}

	public void removeCouponInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("removing coupon applied");
		driver.findElement(removeCoupon).click();
		browserHelper.waitTime(3000);
	}

	public void verifyInvalidCouponError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("InvalidCouponError");
		System.out.println("Verifying invalid error");
		Assert.assertTrue(driver.findElement(couponInvalidError).isDisplayed(),
				"Invalid coupon error is not displayed");
		Assert.assertEquals(driver.findElement(couponInvalidError).getText(), errorText, "Error text not matching");
	}

	public void goBackToSearch() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Clicking on go to search from HD page");
		driver.findElement(By.cssSelector(".search-back-text")).click();
	}

	public List<String> verifyNoDuplicateImagesInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		List<WebElement> elements = driver.findElements(hotelImagesInHD);
		System.out.println("Number of images are :" + elements.size());
		List<String> srcPathList = new ArrayList<String>();
		List<String> srcPathListDupe = new ArrayList<String>();

		for (WebElement element : elements) {
			String srcPath = element.getAttribute("src");
			System.out.println("srcPath : " + srcPath);
			driver.findElement(By.cssSelector(".slick-next.slick-arrow")).click();
			browserHelper.waitTime(1000);
			if (srcPathList.contains(srcPath)) {
				srcPathListDupe.add(srcPath);
			}
			srcPathList.add(srcPath);
		}
		System.out.println(srcPathList.toString());
		System.out.println(srcPathListDupe.toString());
		return srcPathListDupe;
	}

	public boolean areThreeTrilightsItemDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".trilights__item")).size() == 3);
	}

	public String getTrilightsContent() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".trilights__wrapper")).getText();
	}

	public boolean isHotelAccessibilityShown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".access__item")).size() > 0);
	}

	public String getHotelAccessibilityContent() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElement(By.cssSelector(".access")).getText());
	}

	public boolean isSimilarTreebosNearbyShown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean similarTreebosDisplayed = (driver.findElements(By.cssSelector(".similar__title")).size() > 0);
		System.out.println("Similar Treebo hotels displayed : " + similarTreebosDisplayed);
		return similarTreebosDisplayed;
	}

	// public double[] getDistanceOfAllSimilarTreebos(){
	// driver.findElements(By.cssSelector(".similar__treebo.slick-active
	// .similar__name"));
	// }
}
